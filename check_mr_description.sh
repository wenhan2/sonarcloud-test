#!/bin/bash

set -e

# Define the required checkbox text
required_checkbox="- [x] I have fully tested the code and made sure it won't break the main branch"

# Get the MR IID
mr_iid="$CI_MERGE_REQUEST_IID"

# Set the GitLab API URL and the project ID
api_url="https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/merge_requests/$mr_iid"

# Get the MR description
mr_description=$(curl --fail --silent --show-error --header "PRIVATE-TOKEN: glpat-ekhaH1ZXvhApVxwzs9UK" "$api_url" | jq -r '.description')

# Echo the MR description for debugging
echo "Merge Request Description: $mr_description"

# Check if the required checkbox is not checked
if [[ ! "$mr_description" =~ "$required_checkbox" ]]; then
  echo "Error: The required checkbox is not checked. Please check the required checkbox before proceeding."
  exit 1
else
  echo "Required checkbox is checked. Continuing."
fi
