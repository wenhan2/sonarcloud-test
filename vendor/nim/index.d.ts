export enum ErrorCode {
  /**内容包含敏感词 */
  SENSITIVE_WORDS = 20000,
  /**服务端处理回调出错 */
  SERVER_ERROR = 20001,
}

export type NimMessageType = 'text' | 'image' | 'audio' | 'video' | 'file' | 'geo' | 'custom' | 'tip' | 'notification'

export type NimScene = 'p2p' | 'team' | 'superTeam'

export type NimMessageStatus = 'sending' | 'success' | 'fail'

export type Session = {
  id: string
  to: string
  unread: number
  updateTime: string
  scene: NimScene
  lastMsg: LastMsg
}

export type LastMsg = {
  fromNick: string
  from: string
  text: string
  to: string
}

export type Friend = {
  avatar: string
  account: string
  valid: boolean
  nick: string
}

export type FriendUser = {
  account: string
  avatar: string
  birth: string
  createTime: number
  custom: string
  email: string
  gender: string
  nick: string
  sign: string
  tel: string
  updateTime: number
}

export type NimMessage = {
  idClient: string
  scene: NimScene
  from: string
  fromNick: string
  to: string
  time: number
  type: NimMessageType
  sessionId: string
  target: string
  flow: 'in' | 'out'
  status: NimMessageStatus
  /**文本消息内容 */
  text: string
  /**自定义消息的消息内容, 建议封装成JSON格式字符串 */
  content: string
  resend: boolean
  /**扩展字段, 推荐使用JSON格式字符串 */
  custom: string
  localCustom: string
}

export type NimPagination = {
  limit: number
  start: number | null
  end: number | null
}

export type MsgsObj = {
  sessionId: string
  msgs: NimMessage[]
}

export type NimOptions = Partial<{
  debug: boolean
  db: boolean
  appKey: string
  account: string
  token: string
  syncSessionUnread?: boolean
  needReconnect?: boolean
  onsessions: (sessions: Session[]) => void
  onSessionsWithMoreRoaming: () => void
  onupdatesession: (session: Session) => void
  onupdateuser: (user: FriendUser) => void
  onconnect: () => void
  onfriends: (friends: Friend[]) => void
  onusers: (friends: FriendUser[]) => void
  onwillreconnect: (obj: { retryCount: number; duration: number }) => void
  ondisconnect: (err: any) => void
  onerror: (err: any) => void
  onroamingmsgs: (obj: MsgsObj) => void
  onofflinemsgs: (obj: MsgsObj) => void
  onmsg: (msg: NimMessage) => void
}>

export type NimUser = {
  account: string
  nick: string
  avatar: string
}

export type GetUsersOptions = {
  accounts: string[]
  sync?: boolean
  done: (error: RecieveError, users: NimUser[]) => void
}

export type GetUserOptions = {
  account: string
  sync?: boolean
  done: (err: RecieveError, user: NimUser) => void
}

export type GetLocalSessionsOptions = {
  lastSessionId?: string
  limit?: number
  reverse?: boolean
  done: (err: RecieveError, obj: any) => void
}

export type GetLocalSessionOptions = {
  sessionId?: string
  done: (error: RecieveError, obj: Session) => void
}

export type GetLocalMsgsOptions = {
  sessionId?: string
  start?: number
  end?: number
  desc?: boolean
  limit?: number
  done: (error: RecieveError, obj: Pick<NimPagination, 'limit'> & MsgsObj) => void
}

export type GetHistoryMsgsOptions = {
  scene: NimScene
  to: string
  beginTime?: number
  endTime?: number
  lastMsgId?: string
  limit?: number
  done: (error: RecieveError, obj: any) => void
}

export type SendTextOptions = {
  scene: NimScene
  to: string
  text: string
  done: (err: RecieveError, msg: NimMessage) => void
}

export type ResendMsgOptions = {
  msg: NimMessage
  done: (err: RecieveError, obj: NimMessage) => void
}

export type RecieveError = {
  cmd: string
  code: number
  event: any
  message: string
  timetag: number
}

export type UpdateLocalMsgOptions = {
  idClient: string
  localCustom?: string
  done?: (err: RecieveError, obj: NimMessage) => void
}

export class Nim {
  db?: { enable?: boolean }
  mergeSessions: (sessions: Session[], session: Session[] | Session) => Session[]
  mergeFriends: (Friends: Friend[], updatedFriends: Friend[] | Friend) => Friend[]
  mergeUsers: (olds: NimUser[], news: NimUser | NimUser[]) => NimUser[]
  mergeMsgs: (olds: NimMessage[], news: NimMessage | NimMessage[]) => NimMessage[]
  getUsers: (options: GetUsersOptions) => void
  getUser: (options: GetUserOptions) => void
  disconnect: () => void
  destroy: () => void
  setCurrSession: (sessionId: string) => void
  getLocalSession: (options: GetLocalSessionOptions) => void
  getLocalSessions: (options: GetLocalSessionsOptions) => void
  getLocalMsgs: (options: GetLocalMsgsOptions) => void
  getHistoryMsgs: (options: GetHistoryMsgsOptions) => void
  sendText: (options: SendTextOptions) => void
  resetAllSessionUnread: () => void
  resendMsg: (options: ResendMsgOptions) => void
  resetCurrSession: () => void
  updateLocalMsg: (options: UpdateLocalMsgOptions) => void
}

export interface NIM {
  getInstance(args: NimOptions): Nim
  disconnect(): any
}

export interface SDK {
  NIM: NIM
}

declare const SDK: SDK

export default SDK
